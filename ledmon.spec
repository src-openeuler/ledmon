Name:          ledmon
Version:       0.90
Release:       5
Summary:       Utilities for Enclosure LED
License:       GPLv2+
URL:           https://github.com/intel/ledmon
Source0:       https://github.com/intel/ledmon/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
BuildRequires: perl-interpreter perl-podlators sg3_utils sg3_utils-devel systemd-devel
Provides:      ledctl = %{version}-%{release}
Obsoletes:     ledctl = 0.1-1
Requires:      sg3_utils-libs

%description
LED monitor service for storage enclosures.

%package help
Summary:       Help documents for ledmon

%description help
Help documents for ledmon.

%prep
%autosetup -n %{name}-%{version} -p1

%build
make CFLAGS="$RPM_OPT_FLAGS" LDFLAGS="$RPM_LD_FLAGS"

%install
make install INSTALL="%{__install} -p" DESTDIR=$RPM_BUILD_ROOT SBIN_DIR=$RPM_BUILD_ROOT/%{_sbindir} MANDIR=$RPM_BUILD_ROOT%{_mandir}

%files
%doc COPYING README
%{_sbindir}/ledctl
%{_sbindir}/ledmon

%files help
%{_mandir}/*/*

%changelog
* Thu Nov 23 2023 ouuleilei <wangliu@iscas.ac.cn> - 0.90-5
- Fix build error

* Mon Apr 27 2020 wutao <wutao61@huawei.com> - 0.90-4
- Package init
